<?php

use App\Models\Consumer;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ConsumerTest extends TestCase
{

    /**
     * testCreateConsumerSuccess
     *
     * @return void
     */
    public function testCreateConsumerSuccess()
    {
        $fakeConsumer = factory(Consumer::class)->make([
            'user_id' => function () {
                return factory(User::class)->create()->id;
            }
        ])->toArray();

        $this->json('POST', env('APP_URL').'/users/'.$fakeConsumer['user_id'].'/consumers', $fakeConsumer);
        $this->seeStatusCode(201);
        $this->seeJsonStructure();
    }

    /**
     * testUserAlreadyHaveConsumerAccount
     *
     * @return void
     */
    public function testUserAlreadyHaveConsumerAccount()
    {
        $fakeConsumer = factory(Consumer::class)->make([
            'user_id' => function () {
                return DB::table('users')->max('id');
            }
        ])->toArray();

        $this->json('POST', env('APP_URL').'/users/'.$fakeConsumer['user_id'].'/consumers', $fakeConsumer);
        $this->seeStatusCode(422);
        $this->seeJsonStructure();
    }
}
