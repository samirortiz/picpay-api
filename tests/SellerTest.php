<?php

use App\Models\Seller;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class SellerTest extends TestCase
{
    /**
     * testCreateSellerSuccess
     *
     * @return void
     */
    public function testCreateSellerSuccess()
    {
        $fakeSeller = factory(Seller::class)->make([
            'user_id' => function () {
                return factory(User::class)->create()->id;
            },
        ])->toArray();

        $this->json('POST', env('APP_URL').'/users/'.$fakeSeller['user_id'].'/sellers', $fakeSeller);
        $this->seeStatusCode(201);
        $this->seeJsonStructure();
    }

        /**
     * testUserAlreadyHaveSellerAccount
     *
     * @return void
     */
    public function testUserAlreadyHaveSellerAccount()
    {
        $fakeSeller = factory(Seller::class)->make([
            'user_id' => function () {
                return DB::table('users')->max('id');
            }
        ])->toArray();

        $this->json('POST', env('APP_URL').'/users/'.$fakeSeller['user_id'].'/sellers', $fakeSeller);
        $this->seeStatusCode(422);
        $this->seeJsonStructure();
    }
}
