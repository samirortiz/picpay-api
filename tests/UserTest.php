<?php

use App\Models\User;

class UserTest extends TestCase
{
    
    /**
     * testCreateUserSuccess
     *
     * @return void
     */
    public function testCreateUserSuccess()
    {
        $fakeUser = factory(User::class)->make()->toArray();

        $this->json('POST', env('APP_URL').'/users/', $fakeUser);
        $this->seeStatusCode(201);
        $this->seeJsonStructure();
    }
}
