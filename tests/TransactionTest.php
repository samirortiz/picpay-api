<?php

use App\Models\Transaction;

class TransactionTest extends TestCase
{
    /**
     * testAuthorizeTransactionSuccess
     *
     * @return void
     */
    public function testAuthorizeTransactionSuccess()
    {
        $fakeTransaction = factory(Transaction::class)->make()->toArray();
        
        $this->json('POST', env('API_TRANSACTIONS_URL'), $fakeTransaction);
        $this->seeStatusCode(200);
        $this->seeJsonStructure();
    }

    /**
     * testAuthorizeAndCreateTransactionSuccess
     *
     * @return void
     */
    public function testAuthorizeAndCreateTransactionSuccess()
    {
        $fakeTransaction = factory(Transaction::class)->make()->toArray();
        
        $this->json('POST', env('APP_URL').'/transactions', $fakeTransaction);
        $this->seeStatusCode(201);
        $this->seeJsonStructure();
    }
}
