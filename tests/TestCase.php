<?php

use App\Exceptions\Handler;
use Laravel\Lumen\Testing\DatabaseMigrations;
use SebastianBergmann\FileIterator\Factory;

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

}
