<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('payee_id');
            $table->foreign('payee_id')->references('id')->on('accounts')->onDelete('cascade');
            $table->unsignedBigInteger('payer_id');
            $table->foreign('payer_id')->references('id')->on('accounts')->onDelete('cascade');
            $table->decimal('value', 8,2);
            $table->dateTime('transaction_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
