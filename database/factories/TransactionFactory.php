<?php

use App\Models\Transaction;
use Carbon\Carbon;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'payee_id' => function(){
            return DB::table('consumers')->max('id');
        },
        'payer_id' => function(){
            return DB::table('sellers')->min('id');
        },
        'transaction_date' => Carbon::now('america/sao_paulo')->format('Y-m-d H:i:s'),
        'value' => $faker->numberBetween($min = 1, $max = 99)
    ];
});
