<?php

use App\Models\Seller;
use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Seller::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'cnpj' => $faker->numerify('##############'),
        'fantasy_name' => $faker->company,
        'social_name' => $faker->companySuffix 
    ];
});
