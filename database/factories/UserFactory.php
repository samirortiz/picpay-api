<?php

use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'full_name' => $faker->name,
        'cpf' => $faker->unique()->numerify('###########'),
        'email' => $faker->unique()->safeEmail,
        'password' => $faker->numerify('####'),
        'phone_number' => $faker->numerify('###########')
    ];
});
