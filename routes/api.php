<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/* Rota de autorização de transação */
$router->post('/external-transaction', 'TransactionController@externalTransaction');

/* Transactions */
$router->group(['prefix' => '/transactions'], function() use ($router) {
    $router->get('/', 'TransactionController@list');
    $router->get('/{transaction_id}', 'TransactionController@show');
    $router->post('/', 'TransactionController@execute');
});

/* Accounts */
$router->group(['prefix' => '/accounts'], function() use ($router) {
    $router->get('/', 'AccountController@list');
});

/* Sellers */
$router->group(['prefix' => '/users/{user_id}/sellers'], function() use ($router) {
    $router->post('/', 'SellerController@store');
});

/* Consumers */
$router->group(['prefix' => '/users/{user_id}/consumers'], function() use ($router) {
    $router->post('/', 'ConsumerController@store');
});

/* Users */
$router->group(['prefix' => 'users'], function() use ($router) {
    $router->get('/', ['uses' => 'UserController@list', 'as' => 'users.list']);
    $router->get('/{user_id}', 'UserController@show');
    $router->post('/', 'UserController@store');
});

