<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use PDOException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {

        $message = '';
        $code = '';
        $errors = '';

        if($exception instanceof ModelNotFoundException) {
            return response()->json(['code' => 500, 'message' => 'Usuário não encontrado!'], 500);
        }
        
        if($exception instanceof PDOException) {
            return response()->json(['code' => 500, 'message' => 'Erro interno do servidor!'], 500);
        }

        if($exception instanceof HttpException) {
            switch ($exception->getCode()) {
                case 401: 
                    $code = $exception->getCode();
                    $message = 'Não autorizado!';
                break;
                default: 
                    $code = 404;
                    $message = 'Recurso não encontrado!';
                break;
            }
            return response()->json(['code' => $code, 'message' => $message], $code);
        }

        if($exception instanceof ValidationException) {
            switch ($exception->getCode()) {
                case 401: 
                    $code = $exception->getCode();
                    $message = 'Não autorizado!';
                    $errors = json_decode($exception->getResponse()->getContent());
                break;
                default:
                    $code = 422;
                    $message = 'Erro na validação dos dados!';
                    $errors = json_decode($exception->getResponse()->getContent());
                break;
            }
            return response()->json(['code' => $code, 'message' => $message, 'errors' => $errors], $code);
        }

        return parent::render($request, $exception);
    }
}
