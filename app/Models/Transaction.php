<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $guarded = ['q'];
    protected $fillable = ['payee_id', 'payer_id', 'transaction_date', 'value'];

    public $timestamps = false;

}
