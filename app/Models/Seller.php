<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    const ACCOUNT_TYPE = 'S';

    protected $guarded = ['q'];
    protected $fillable  = ['user_id', 'username', 'cnpj', 'fantasy_name', 'social_name']; 

    public $timestamps = false;
        
}
