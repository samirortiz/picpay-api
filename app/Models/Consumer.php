<?php

namespace App\Models;

use App\Enums\AccountType;
use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{

    protected $guarded = ['q'];
    protected $fillable  = ['user_id', 'username']; 

    public $timestamps = false;
    
}
