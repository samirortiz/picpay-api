<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $guarded = ['q'];
    protected $fillable = ['full_name', 'cpf', 'email', 'password', 'phone_number']; 

    public $timestamps = false;
    
    public function account() 
    {
        return $this->hasMany(Account::class);
    }

    public function seller() 
    {
        return $this->hasOne(Seller::class);
    }

    public function consumer() 
    {
        return $this->hasOne(Consumer::class);
    }

}
