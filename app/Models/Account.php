<?php

namespace App\Models;

use App\Enums\AccountType;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{

    protected $guarded = ['q'];
    protected $fillable = ['user_id', 'owner_id', 'account_type'];

    public $timestamps = false;

    public function consumer() 
    {
        return $this->belongsTo(Consumer::class, 'owner_id');
    }

    public function seller() 
    {
        return $this->belongsTo(Seller::class, 'owner_id');
    }

}
