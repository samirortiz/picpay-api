<?php

namespace App\Http\Services;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class TransactionService
{

    /**
     * list
     *
     * @return Collection
     */
    public static function list() : Collection 
    {
        return Transaction::all();
    }

    /**
     * show
     *
     * @param  integer $transaction_id
     *
     * @return Transaction
     */
    public function show(int $transaction_id) : Transaction
    {
        $transaction = Transaction::find($transaction_id);

        if(!$transaction) {
            abort(401);
        }

        return $transaction;
    }

    /**
     * store
     *
     * @param  array $data
     *
     * @return Transaction
     */
    public static function store(array $data) : Transaction
    {
        $data['transaction_date'] = Carbon::now('america/sao_paulo')->format('Y-m-d H:i:s');
    
        return Transaction::create($data);
    }


}