<?php

namespace App\Http\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Collection;

class UserService
{

    /**
     * list
     *
     * @param  array $filter
     *
     * @return Collection
     */
    public function list(array $filter) : Collection 
    {
        $query = User::query();

        if($filter['q'] == '/users' || $filter['q'] == '/users/') {
            return $query->orderBy('full_name')->get();
        }

        $users = $query->select('users.*')
            ->leftJoin('consumers as c', 'c.user_id', '=', 'users.id')
            ->leftJoin('sellers as s', 's.user_id', '=', 'users.id')
            ->whereRaw('s.username like "'.$filter['q'].'%"')
            ->orWhereRaw('c.username like "'.$filter['q'].'%"')
            ->orWhereRaw('users.full_name like "'.$filter['q'].'%"')
            ->get();

        return $users;
    }

    /**
     * show
     *
     * @param  integer $userId
     *
     * @return User
     */
    public function show(int $userId) : UserResource 
    {
        return new UserResource(User::with('account.consumer')
            ->with('account.seller')
            ->where('id', $userId)
            ->first());
    }

    /**
     * store
     *
     * @param  array $data
     *
     * @return User
     */
    public function store(array $data) : User 
    {
        return User::create($data);
    }
}