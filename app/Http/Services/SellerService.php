<?php

namespace App\Http\Services;

use App\Enums\AccountType;
use App\Models\Seller;
use Exception;
use Illuminate\Support\Facades\DB;

class SellerService
{

    /**
     * store
     *
     * @param  array $data
     *
     * @return Seller
     */
    public function store(array $data) : Seller 
    {
        try {
            $seller = app(Seller::class);

            DB::beginTransaction();

            $seller = Seller::create($data);

            $accountData = [
                'owner_id' => $seller->id,
                'user_id' => $seller->user_id,
                'account_type' => AccountType::SELLER
            ];
        
            AccountService::store($accountData);
            DB::commit();

            return $seller;
            
        }catch(Exception $e) {
            DB::rollback();
            abort(404);
        }
    }
}