<?php

namespace App\Http\Services;

use App\Models\Account;
use Illuminate\Support\Collection;

class AccountService
{

    /**
     * list
     *
     * @return Collection
     */
    public function list() : Collection 
    {
        return Account::all();
    }

    /**
     * store
     *
     * @param  array $data
     *
     * @return Account
     */
    public static function store(array $data) : Account 
    {
        return Account::create($data);
    }


}