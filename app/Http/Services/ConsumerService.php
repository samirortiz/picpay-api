<?php

namespace App\Http\Services;

use App\Enums\AccountType;
use App\Models\Consumer;
use Exception;
use Illuminate\Support\Facades\DB;

class ConsumerService
{

    /**
     * store
     *
     * @param  array $data
     *
     * @return Consumer
     */
    public function store(array $data) : Consumer 
    {
        try {
            $consumer = app(Consumer::class);

            DB::beginTransaction();

            $consumer = Consumer::create($data);  

            $accountData = [
                'owner_id' => $consumer->id,
                'user_id' => $consumer->user_id,
                'account_type' => AccountType::CONSUMER
            ];
        
            AccountService::store($accountData);
            DB::commit();

            return $consumer;
            
        }catch(Exception $e) {
            DB::rollback();
            abort(404);
        }
    }
}