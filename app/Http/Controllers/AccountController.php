<?php

namespace App\Http\Controllers;

use App\Http\Resources\AccountResource;
use App\Http\Services\AccountService;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AccountController extends Controller
{
    private $accountService;

    /**
     * __construct
     *
     * @param  AccountService $accountService
     *
     * @return void
     */
    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * list
     *
     * @return ResourceCollection
     */
    public function list() : ResourceCollection
    {
        return AccountResource::collection($this->accountService->list());
    }

}
