<?php

namespace App\Http\Controllers;

use App\Http\Resources\ConsumerResource;
use App\Http\Services\ConsumerService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ConsumerController extends Controller
{
    private $consumerService;

    /**
     * __construct
     *
     * @param  mixed $consumerService
     *
     * @return void
     */
    public function __construct(ConsumerService $consumerService)
    {
        $this->consumerService = $consumerService;
    }

    /**
     * store
     *
     * @param  Request $request
     * @param  int $user_id
     *
     * @return ConsumerResource
     */
    public function store(Request $request, $user_id) : ConsumerResource
    {
        $request['user_id'] = $user_id ?? null;

        $this->validate($request, [
            'user_id'   => 'bail|required|integer|unique:consumers|exists:users,id',
            'username'  => 'required|unique:consumers|unique:sellers',
        ]);

        $consumer = $this->consumerService->store($request->all());

        return new ConsumerResource($consumer);
    }

}
