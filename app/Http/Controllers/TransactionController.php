<?php

namespace App\Http\Controllers;

use App\Http\Resources\TransactionResource;
use App\Http\Services\TransactionService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    private $transactionService;

    /**
     * __construct
     *
     * @param  TransactionService $transactionService
     *
     * @return void
     */
    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * list
     *
     * @return ResourceCollection
     */
    public function list() : ResourceCollection
    {
        return TransactionResource::collection($this->transactionService->list());
    }

    /**
     * show
     *
     * @param  integer $transaction_id
     *
     * @return TransactionResource
     */
    public function show($transaction_id) : TransactionResource
    {
        return new TransactionResource($this->transactionService->show($transaction_id));
    }

    /**
     * execute - Chamada para API externa de autorização de transações
     *
     * @param  \Illuminate\Http\Request
     *
     * 
     */
    public function execute(Request $request) 
    {
        try {
            $post = Request::create(env('API_TRANSACTIONS_URL'), 'POST', $request->all());
        
            $response = app()->handle($post);
    
            if($response->getStatusCode() != 200) {
                if($response->getStatusCode() == 500){
                    return response()->json(['code' => $response->getStatusCode(), 'message' => 'Erro interno do servidor!'], $response->getStatusCode());    
                }
                return response()->json([json_decode($response->getContent())], $response->getStatusCode());
            }

            return new TransactionResource($this->transactionService->store($request->all()));

        } catch (Exception $e) {
            abort(404);
        }
    }
    


    // ROTA DO SERVIÇO EXTERNO ENTRA AQUI
    /**
     * externalTransaction
     *
     * @param  Request $request
     *
     * @return JsonResponse
     */
    public function externalTransaction(Request $request) 
    {
        $validator = Validator::make($request->all(), [
            'payee_id'     => 'required|integer|gt:0|exists:accounts,id',
            'payer_id'     => 'required|integer|gt:0|exists:accounts,id',
            'value'        => 'required|numeric|gt:0|lt:100'
        ]);
        
        if($validator->fails()) {
            return response()->json(['code' => 401, 'message' => 'Não autorizado!', 'errors' => json_encode($validator->messages())], 401);
        }

        return response()->json(['code' => 200, 'message' => 'Autorizado!']);
    }

}
