<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Services\UserService;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserController extends Controller
{

    private $userService;

    /**
     * __construct
     *
     * @param  mixed $userService
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * list
     *
     * @param  Request $request
     *
     * @return ResourceCollection
     */
    public function list(Request $request) : ResourceCollection 
    {
        $users = $this->userService->list($request->all());
        
        if(!count($users)) {
            abort(404);
        }
        
        return UserResource::collection($users);
    }

    /**
     * show
     *
     * @param  integer $user_id
     *
     * @return UserResource
     */
    public function show($user_id) : UserResource 
    {
        $user = $this->userService->show($user_id);

        return new UserResource($user);
    }

    /**
     * store
     *
     * @param  Request $request
     *
     * @return UserResource
     */
    public function store(Request $request) : UserResource 
    {    
        $this->validate($request, [
            'full_name'     => 'required|string',
            'cpf'           => 'required|size:11|unique:users',
            'email'         => 'required|email|unique:users',
            'password'      => 'required|digits_between:4,8',
            'phone_number'  => 'numeric'
        ]);

        $user = ($this->userService->store($request->all()));

        return new UserResource($user);
    }

}
