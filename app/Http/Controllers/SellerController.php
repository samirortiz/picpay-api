<?php

namespace App\Http\Controllers;

use App\Http\Resources\SellerResource;
use App\Http\Services\SellerService;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    private $sellerService;

    /**
     * __construct
     *
     * @param  mixed $sellerService
     *
     * @return void
     */
    public function __construct(SellerService $sellerService)
    {
        $this->sellerService = $sellerService;
    }

    /**
     * store
     *
     * @param  Request $request
     * @param  integer $user_id
     *
     * @return SellerResource
     */
    public function store(Request $request, $user_id) : SellerResource
    {    
        $request['user_id'] = $user_id;

        $this->validate($request, [
            'user_id'   => 'bail|required|integer|unique:sellers|exists:users,id',
            'username'  => 'required|unique:consumers|unique:sellers',
            'cnpj' => 'required|size:14|unique:sellers',
            'fantasy_name' => 'required',
            'social_name' => 'required',
        ]);

        $seller = $this->sellerService->store($request->all());

        return new SellerResource($seller);
    }

}
