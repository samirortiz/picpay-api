<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{

    protected $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) : array
    {
        if (is_null($this->resource)) {
            abort(404);
        }

        return [
            'id' => $this->id,
            'payee_id' => $this->payee_id,
            'payer_id' => $this->payer_id,
            'transaction_date' => $this->transaction_date,
            'value' => $this->value
        ];
    }
}