<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    protected $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) : array
    {
        if (is_null($this->resource)) {
            abort(404);
        }

        return [
            'id' => $this->id,
            'cpf' => $this->cpf,
            'email' => $this->email,
            'full_name' => $this->full_name,
            'password' => $this->password,
            'phone_number' => $this->phone_number,
            'accounts' => [
                'consumer' => new ConsumerResource($this->consumer) ?? null,
                'seller' => new SellerResource($this->seller) ?? null
            ]
        ];
    }

    
}