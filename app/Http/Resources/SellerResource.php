<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SellerResource extends JsonResource
{

    protected $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) : array
    {
        if (is_null($this->resource)) {
            abort(404);
        }

        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'username' => $this->username,
            'cnpj' => $this->cnpj,
            'fantasy_name' => $this->fantasy_name,
            'social_name' => $this->social_name,
        ];
    }
}