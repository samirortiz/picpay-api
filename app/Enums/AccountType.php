<?php

namespace App\Enums;

use App\Models\Consumer;
use BenSampo\Enum\Enum;

final class AccountType extends Enum
{
    const CONSUMER = 'C';
    const SELLER = 'S';


}